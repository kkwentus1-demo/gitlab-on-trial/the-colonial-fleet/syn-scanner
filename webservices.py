# create a webservice using tornado that allows a user to login, run a security scan, and review the scan results.
# each action (login, run scan, and review results) should be their own resource in the webservice
# the webservice should interact with syn-scanner.py to run a security scan
# results of the scan should be stored in a mysql database ans results should be retrieved from it
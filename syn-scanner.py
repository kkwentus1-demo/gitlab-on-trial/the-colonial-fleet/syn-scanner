# /usr/bin/python
#############################################
# syn_scanner v0.1
# author: david desanto
# email:  david.desanto(at)gmail(dot)com
#############################################

# import needed modules
from optparse import OptionParser
import logging, socket, sys

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *


# scanner function.  repeated chunk of code needed for scanning the amount of IPs requested
def scanner(dest_ip, start_port, end_port):
    open_ports = []  # build a list to store open ports in
    try:
        ans, unans = sr(IP(dst=dest_ip) / TCP(dport=(int(start_port), int(end_port)), flags="S"), verbose=0, retry=1,
                        timeout=2)  # call sr function of scapy to scan for open ports
    except KeyboardInterrupt:  # catch any ^c commands to exit clean.
        sys.stdout.write("\nscan interrupted by user.  exiting...\n\n")
        exit()
    except:  # if the sr function fails to send packets, it may be a privileges issue.
        sys.stdout.write("\n\nscan failed. confirm you have correct privileges.\n\n")
        exit()
    for s, r in ans:  # for sent and received packets in answered list
        if s[TCP].dport == r[TCP].sport and r[
            TCP].flags == 18:  # if source port of sent packet = destination port of received packet and SA flags are set
            open_ports.append(str(s[TCP].dport))  # add to open_ports list
    if len(open_ports) == 0:  # if no ports are found
        sys.stdout.write("no open ports found at " + scan_ip + ".\n")
    else:  # print out found open ports
        sys.stdout.write("the following ports are open at " + scan_ip + ".  all other ports filtered/closed.\n")
        sys.stdout.write("port \tservice name\n")
        for port in open_ports:  # extract each port from open_ports list
            sys.stdout.write(port + '\t')
            try:
                portname = socket.getservbyport(int(port), 'tcp')  # query socket module to get name of service
                sys.stdout.write(portname + '\n')
            except:
                sys.stdout.write("other\n")


# get command line options and their values. validate input and return validated input to main.
def get_stuff():
    # build options
    usage = "usage: %prog [options]"
    parser = OptionParser(usage, version="%prog v0.1")
    parser.add_option("-i", "--ip-address", dest="dest_ip", type="string",
                      help="start ip address to be scanned (i.e. 10.1.1.1)")
    parser.add_option("-d", "--destintion-hostnanme", dest="host", type="string",
                      help="hostname to be scanned (i.e. registry.gitlab.com)")
    parser.add_option("-n", "--number_hosts", dest="num_hosts", type="string",
                      help="number of hosts to be scanned (i.e. 1000)")
    parser.add_option("-p", "--port-range", dest="port_range", type="string",
                      help="port range to be scanned (1 port or range (80-90))")

    (options, args) = parser.parse_args()  # extract values
    if not options.dest_ip or not options.port_range or not options.num_hosts or not options.host:  # check and confirm all needed values are present
        sys.stdout.write("\nthere are not enough options set.  see below for usage.\n\n")
        parser.print_help()
        sys.exit()
    num_hosts = options.num_hosts  # copy value into num_hosts
    host = options.host # copy value into host
    host_dns = socket.gethostbyaddr(host)
    host_ip = str(host_dns[2])
    dest_ip = host_ip[2:-2]  # copy value into dest_ip
    #dest_ip = options.dest_ip  # copy value into dest_ip
    port_range = options.port_range  # copy port_range into port_range
    multi_port = port_range.find("-")  # search port_range to see if user requested multiple ports
    if multi_port == -1:  # if -1, then no - was found
        # set start_port and end_port to same value
        start_port = port_range
        end_port = port_range
        try:
            int(start_port)  # confirm port number is an integer
        except ValueError:  # catch error if it is not
            sys.stdout.write("\nthe port number entered is not numerical.  please fix.\n\n")
            exit()
    else:
        start_port = port_range[:multi_port]  # copy up to - into start_port
        end_port = port_range[multi_port + 1:]  # copy from after - to end of port_range into end_port
        try:  # check and see if start_port and end_port are integers
            int(start_port)
            int(end_port)
        except ValueError:  # if not, catch the error
            sys.stdout.write("\nthe port range entered is not numerical.  please fix.\n\n")
            exit()
        if start_port > end_port:  # confirm port range is ascending
            sys.stdout.write("\nerror: starting port is greater than ending port.  please fix.\n\n")
            exit()
    if int(start_port) > 65535 or int(end_port) > 65535:  # confirm port value isn't too high
        sys.stdout.write("\nerror: starting port or ending port is greater than 65535.  please fix.\n\n")
        exit()
    if int(start_port) < 0 or int(end_port) < 0:  # confirm port value isn't too low
        sys.stdout.write("\nerror: starting port or ending port is less than 0.  please fix.\n\n")
        exit()
    if dest_ip.count('.') != 3:  # confirm a four-tuple was entered
        sys.stdout.write("\nthe ip address entered is invalid.  only enter addresses in X.X.X.X format\n\n")
        exit()
    oct1, oct2, oct3, oct4 = dest_ip.split('.')  # split dest_ip into its individual octets
    try:  # confirm all octets are integers
        int(oct1)
        int(oct2)
        int(oct3)
        int(oct4)
    except ValueError:  # if not, catch the error
        sys.stdout.write("\nthe ip address contains letters.  please fix.\n\n")
        exit()
    if int(oct4) > 255 or int(oct3) > 255 or int(oct2) > 255 or int(
            oct1) > 255:  # confirm dest_ip is valid and within ipv4 range
        sys.stdout.write("\nerror: ip address contains incorrect values > 255.  please fix.\n\n")
        exit()
    if int(oct4) < 0 or int(oct3) < 0 or int(oct2) < 0 or int(
            oct1) <= 0:  # confirm dest_ip is valid and within ipv4 range
        sys.stdout.write("\nerror: ip address contains incorrect values < 0 or 0 for oct1.  please fix.\n\n")
        exit()
    try:  # confirm num_hosts is an integer
        int(num_hosts)
    except ValueError:  # if not, catch the error
        sys.stdout.write("\nthe number of hosts contains letters.  please fix.\n\n")
        exit()
    if int(num_hosts) <= 0:  # confirm there is an amount of hosts to scan.
        sys.stdout.write("\nthe number of hosts to be scanned is <= 0.  please fix.\n\n")
        exit()
    return num_hosts, dest_ip, host, start_port, end_port;  # return options to main


# the main function of syn_scanner
if __name__ == "__main__":
    try:  # attempt to run all of these commands
        num_hosts, dest_ip, host, start_port, end_port = get_stuff()  # call get_stuff and assign returned values to num_hosts, dest_ip, start_port, end_port
        # confirm user input before starting
        sys.stdout.write("\nbelow is the information entered:\n\n")
        sys.stdout.write("ip address to start:\t" + dest_ip + "\n")
        sys.stdout.write("ip addresses to scan:\t" + num_hosts + "\n")
        if int(start_port) == int(end_port):
            sys.stdout.write("port number to scan:\t" + start_port + "\n\n")
        else:
            sys.stdout.write("port numbers to scan:\t" + start_port + "-" + end_port + "\n\n")
        confirmed = input("enter 'yes' to confirm, 'no' to exit --> ")
        while confirmed != "yes" and confirmed != "no":  # while user has not entered yes or no
            confirmed = input("enter 'yes' to confirm, 'no' to exit --> ")
        if confirmed == "no":  # if no, exit clean
            sys.stdout.write("\nexiting without running scan.  have a good day.\n\n")
            exit()
        oct1, oct2, oct3, oct4 = dest_ip.split('.')  # split dest_ip into individual octets
        for i in range(int(num_hosts)):  # repeat until the number of hosts requested to be scanned is completed
            # even though uncommon, ipv4 addresses can end in 0 or 255, see http://en.wikipedia.org/wiki/IPv4#Addresses_ending_in_0_or_255
            if int(oct4) >= 256:  # if oct4 is 256, reset to 0 and increase oct3
                oct4 = "0"
                oct3 = str(int(oct3) + 1)
            if int(oct3) >= 256:  # if oct3 is 256, reset to 0 and increase oct2
                oct3 = "0"
                oct2 = str(int(oct2) + 1)
            if int(oct2) >= 256:  # if oct2 is 256, reset to 0 and increase oct1
                oct2 = "0"
                oct1 = str(int(oct1) + 1)
            if int(oct1) >= 256:  # if requested amount of hosts exceeds ipv4 range, exit clean
                sys.stdout.write("\nerror: first octet of ip address no longer valid. exiting scan...\n\n")
                exit()
            scan_ip = '.'.join([oct1, oct2, oct3, oct4])  # rebuild ip address as a string
            sys.stdout.write("\nstarting scan of host " + str(i + 1) + " at " + scan_ip + "...\n\n")
            scanner(scan_ip, start_port, end_port)  # call scanner function
            oct4 = str(int(oct4) + 1)  # increment ip address
        sys.stdout.write("\ncompleted scan of " + num_hosts + " hosts.  thank you for using syn_scanner.\n\n")
    except KeyboardInterrupt:  # and catch keyboard interrupt like ^c
        sys.stdout.write("\n\nscan interrupted by user.  exiting...\n\n")
        exit()
